package week1;
import java.util.Scanner;

import org.omg.Messaging.SyncScopeHelper;

public class Voltage_Simulator {
	
	static double v;
	static double vi;
	static double r;
	static double c;
	static double t;
	static Scanner sc;
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice = 1;
		while(choice != 0) {
			System.out.println("Please select one of th following: \n "
					+ "(1) Calculate Voltage \n "
					+ "(2) Calculate Applied Voltage \n "
					+ "(3) Calculate Resistance \n "
					+ "(4) Calculate Capacitance \n "
					+ "(5) Calculate Time to reach a set voltage \n "
					+ "(6) Simulator \n "
					+ "(0) Exit \n "
					+ "------------------------------------------");
			System.out.println("Option:");
			choice = sc.nextInt();
			
			switch(choice) {
			case 0:
				System.out.println("Exiting...");
				
				break;
			case 1:
				System.out.println("Calculate voltage selected.");
				
				System.out.println("Enter the Resistance (kiloOhms):");
				r = (sc.nextDouble())*(Math.pow(10,3));
				System.out.println("Enter the Applied Voltage (volts) :");
				vi = sc.nextDouble();
				System.out.println("Enter the Capacitance:");
				c = (sc.nextDouble())*(Math.pow(10,-6));
				System.out.println("Enter the Time (microseconds):");
				t = (sc.nextDouble())*(Math.pow(10,-6));
				
				Voltage(vi, r, c, t);
				
				break;
			case 2:
				System.out.println("Calculate Applied Voltage selected.");
				System.out.println("Enter the Voltage (milliVolts):");
				v = (sc.nextDouble())*(Math.pow(10,-3));
				System.out.println("Enter the Resistance (kiloOhms):");
				r = (sc.nextDouble())*(Math.pow(10,3));
				System.out.println("Enter the Capacitance:");
				c = (sc.nextDouble())*(Math.pow(10,-6));
				System.out.println("Enter the Time (microseconds):");
				t = (sc.nextDouble())*(Math.pow(10,-6));
				
				AppliedVoltage(v, r, c, t);
				
				break;
			case 3:
				System.out.println("Calculate Resistance selected.");
				System.out.println("Enter the Voltage (milliVolts):");
				v = (sc.nextDouble())*(Math.pow(10,-3));
				System.out.println("Enter the Applied Voltage (volts) :");
				vi = sc.nextDouble();
				System.out.println("Enter the Capacitance:");
				c = (sc.nextDouble())*(Math.pow(10,-6));
				System.out.println("Enter the Time (microseconds):");
				t = (sc.nextDouble())*(Math.pow(10,-6));
				
				Resistance(v, vi, c, t);
				
				break;
			case 4:
				System.out.println("Calculate Capacitance selected.");
				
				System.out.println("Calculate Resistance selected.");
				System.out.println("Enter the Voltage (milliVolts):");
				v = (sc.nextDouble())*(Math.pow(10,-3));
				System.out.println("Enter the Applied Voltage (volts) :");
				vi = sc.nextDouble();
				System.out.println("Enter the Resistance (kiloOhms):");
				r = (sc.nextDouble())*(Math.pow(10,3));
				System.out.println("Enter the Time (microseconds):");
				t = (sc.nextDouble())*(Math.pow(10,-6));
				
				Capacitance(v, vi, r, t);
				
				break;
			case 5:
				System.out.println("Calculate Time to reach a set voltage selected.");
				
				System.out.println("Enter the Voltage (milliVolts):");
				v = sc.nextDouble()*(Math.pow(10,-3));
				System.out.println("Enter the Resistance (kiloOhms):");
				r = (sc.nextDouble())*(Math.pow(10,3));
				System.out.println("Enter the Applied Voltage (volts) :");
				vi = sc.nextDouble();
				System.out.println("Enter the Capacitance:");
				c = (sc.nextDouble())*(Math.pow(10,-6));
				
				Time(v, vi, r, c);
				
				break;
			case 6:
				System.out.println("Simulator selected.");
				
				System.out.println("Enter the Resistance (kiloOhms):");
				r = (sc.nextDouble())*(Math.pow(10,3));
				System.out.println("Enter the Applied Voltage (volts) :");
				vi = sc.nextDouble();
				System.out.println("Enter the Capacitance:");
				c = (sc.nextDouble())*(Math.pow(10,-6));
				
				Simulator(vi, r, c);
				
				break;
			default:
				break;
			}
		}
		
	}
	
	static void Voltage(double vi, double r, double c, double t) {
		double result = vi * ( 1 - Math.pow(Math.E, (-t/(r*c))) ) ;
		System.out.println("The voltage is "+result+" volts.\n");
	}
	static void AppliedVoltage(double v, double r, double c, double t) {
		double result = v / ( 1 - Math.pow(Math.E, (-t/(r*c))) ) ;
		System.out.println("The applied voltage is "+result+" volts.\n");
	}
	static void Resistance(double v, double vi, double c, double t) {
		double result = t / (c*(-Math.log((-v/vi)+1))) ;
		System.out.println("The resistance is "+result+" ohms.\n");
	}
	static void Capacitance(double v, double vi, double r, double t) {
		double result = t / (r*(-Math.log((-v/vi)+1))) ;
		System.out.println("The capacitance is "+result+" farads.\n");
	}
	static void Time(double v, double vi, double r, double c) {
		double result = r*c*(-Math.log((-v/vi)+1)) ;
		System.out.println("The time is "+result*Math.pow(10, 6)+" seconds.\n");
	}
	static void Simulator(double vi, double r, double c) {
		double time = 1.0;
		
		double temp;
		System.out.println("Time(micro-Seconds):"+"		"+ "Voltage(milli-Volts)");
		
		while(time*Math.pow(10,-6) <= 1) {
			temp = vi * ( 1 - Math.pow(Math.E, (-(time *Math.pow(10,-6))/(r*c))));
			System.out.println(time + "			"+temp);
			time *= 10;
		}
	}
}
