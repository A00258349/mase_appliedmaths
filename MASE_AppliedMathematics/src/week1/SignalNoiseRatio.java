package week1;
import java.util.Scanner;

public class SignalNoiseRatio {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("");
		double snr = sc.nextDouble();
		double b = sc.nextDouble();
		
		snr = Math.pow(10, snr/10);
		
		double c = b*Math.log(2)*1+snr;
		System.out.println(c);
	}

}
