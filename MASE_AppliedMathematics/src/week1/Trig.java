package week1;
import java.util.Scanner;

public class Trig {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the distance from the object (metres):");
		double distance = sc.nextDouble();
		System.out.println("Enter the angle of elevation between the ground and the object (degrees): ");
		double angle = sc.nextDouble();
		System.out.format("The height of the object is %.2f",distance*Math.tan(Math.toRadians(angle)));
		
	}

}
